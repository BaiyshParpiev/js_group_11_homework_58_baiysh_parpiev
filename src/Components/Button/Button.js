import React from 'react';
import './Button.css';

const Button = props => (
    <button onClick={props.onClick} className={['btn', `${'btn-'+ props.type}`].join(' ')}>{props.label}</button>
);

export default Button;