import React from 'react';
import Button from "../Components/Button/Button";
import './Alert.css';

const Alert = props => (
    <div  className={['alert', `${'alert-' + props.type}`].join(' ')}>
        {props.children}
        {!props.dismiss ? null : <Button onClick={type => props.dismiss(props.type)} label="x" type="primary"/>}
    </div>
);

export default Alert;