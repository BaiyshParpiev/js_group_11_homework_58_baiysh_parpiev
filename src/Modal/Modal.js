import React from 'react';
import './Modal.css';
import Button from "../Components/Button/Button";

const Modal = props => {
    return (
        <>
            <div
                className="Modal"
                style={{
                    transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                    opacity: props.show ? '1' : '0',
                }}
            >
                <div>
                    <h3 className="modal-title">{props.title}</h3>
                </div>
                {props.children}
                <div>
                    {props.buttons.map(map => (
                        <Button key={map.id} type={map.type} label={map.label} onClick={map.click}/>
                    ))}
                </div>
            </div>
        </>
    );
};

export default Modal;