import React, {useState} from "react";
import Modal from "../Modal/Modal";
import Alert from "../Alert/Alert";
import Button from "../Components/Button/Button";
import {nanoid} from 'nanoid'

const App = () => {
    const [purchasing, setPurchasing] = useState(false);
    const [alertStyle, setAlertStyle] = useState([]);


    const purchaseHandler = () => {
        setPurchasing(true);
    };

    const cancelPurchaseHandler = () => {
        setPurchasing(false);
    };

    const click = () => {
        alert("You will continue!");
    }

    const dismiss = type => {
        setAlertStyle(alert => alert.map(a=> {
            if(a.type === type){
                return {...a, type: 'block-none'}
            }
            return a;
        }));
    }

    const showAlert = () => {
        setAlertStyle(
            [
                {type: "warning", label: "This is warning Alert" ,dismiss: dismiss, id: nanoid()},
                {type: "success", label: "This is success Alert",id: nanoid()},
                {type: "danger", label: "This is danger Alert",id: nanoid()},
            ]
        )
    }


    const alertShow = alertStyle.map(a => (
        <Alert key={a.id} type={a.type} dismiss={a.dismiss}>{a.label}</Alert>
    ))

    return (
        <>
            <p>Level 1</p>
            <Modal
                show={purchasing}
                onClick={cancelPurchaseHandler}
                title="Some kinda modal title"
                buttons={
                    [{type: 'primary', label: 'Continue', click: click, id:nanoid()},
                        {type: 'danger', label: 'Close', click: cancelPurchaseHandler, id:nanoid()}]
                }
            ><h2>This is modal content</h2></Modal>
            <Button onClick={purchaseHandler} label="Launch demo modal" type="primary"/>
            <p>Level 2</p>
            {alertShow}
            <Button onClick={showAlert} label="Launch demo Alert" type="primary"/>
        </>
    )
};

export default App;
